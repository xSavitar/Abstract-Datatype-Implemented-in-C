#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include"point.h"

float randFloat()	/* random numbers between 0 & 1 for x or y coord */
{
	return 1.0 * rand()/RAND_MAX;
}

int main(int argc, char * argv[])
{
	float d = atof(argv[2]);
	int i, j, cnt = 0, N = atoi(argv[1]);
	/*Enter declaration for array a here*/
	point a[N];
	for(i = 0; i < N; i++){
		a[i].x = randFloat();
		a[i].y = randFloat();
	}
	for(i = 0; i < N; i++){
		for(j = i + 1; j < N; j++){
			/* Compare distance btw two points with d and */
			if(distance(a[i], a[j]) < d)
			/* Update if necessary */
			cnt++;
		}
	}
	printf("%d edges shorter than %.2f\n", cnt, d);
	return 0;
}
